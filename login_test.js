Feature('login');

Scenario('Login com sucesso',  ({ I }) => {
    
    I.amOnPage('http://automationpratice.com.br/')
    I.click('Login')
    I.waitForText('Login', 5)
    I.fillField('#user', 'lucassr91@live.com')
    I.fillField('#password', '123456')
    I.click('#btnLogin')
    I.waitForText('Login realizado', 5)
   
}).tag('@sucesso')

Scenario('Tentando Logar digitando apenas o e-mail',  ({ I }) => {
    
    I.amOnPage('http://automationpratice.com.br/')
    I.click('Login')
    I.waitForText('Login', 5)
    I.fillField('#user', 'lucassr91@live.com')
    I.click('#btnLogin')
    I.waitForText('Senha inválida.', 5)

}).tag('@falha')


Scenario('Tentando logar sem digitar e-mail e senha',  ({ I }) => {
    
    I.amOnPage('http://automationpratice.com.br/')
    I.click('Login')
    I.waitForText('Login', 5)
    I.click('#btnLogin')
    I.waitForText('E-mail inválido.', 5)

}).tag('@falha')

Scenario('Tentando Logar digitando apenas a senha',  ({ I }) => {
    
    I.amOnPage('http://automationpratice.com.br/')
    I.click('Login')
    I.waitForText('Login', 5)
    I.fillField('#password', '123456')
    I.click('#btnLogin')
    I.waitForText('E-mail inválido.', 5)

}).tag('@falha')

Scenario('Tentando Logar com senha inválida',  ({ I }) => {
    
    I.amOnPage('http://automationpratice.com.br/')
    I.click('Login')
    I.waitForText('Login', 5)
    I.fillField('#user', 'lucassr91@live.com')
    I.fillField('#password', '12345')
    I.click('#btnLogin')
    I.waitForText('Senha inválida.', 5)
   
}).tag('@falha')

Scenario('Tentando Logar com e-mail inválido',  ({ I }) => {
    
    I.amOnPage('http://automationpratice.com.br/')
    I.click('Login')
    I.waitForText('Login', 5)
    I.fillField('#user', 'lucassr91live.com')
    I.fillField('#password', '123456')
    I.click('#btnLogin')
    I.waitForText('E-mail inválido.', 5)
   
}).tag('@falha')